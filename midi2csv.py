#!/usr/bin/python
import mido
import os
import sys, getopt
import csv

from mido import Message, MidiFile, MidiTrack, MetaMessage

def main(argv):
    inputdir = ''
    outputdir = ''
    ismidi2csv = True
    try:
        opts, args = getopt.getopt(argv,"hi:o:r",["help","inputdir=","outputdir=","reverse"])
    except getopt.GetoptError:
        printUsage();
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            printUsage();
            sys.exit()
        elif opt in ("-i", "--inputdir"):
            inputdir = arg
        elif opt in ("-o", "--outputdir"):
            outputdir = arg
        elif opt in ("-r", "--reverse"):
            ismidi2csv = False
    if inputdir == '' or outputdir == '':
        printUsage();
    else:
        if ismidi2csv:
            midi2csv(inputdir, outputdir)
        else:
            csv2midi(inputdir, outputdir)



def printUsage():
    print ('usage: midi2csv.py [options] ... [-i --inputdir | -o --outputdir] [arg] ...\nOptions and arguments:\n-i --inputdir\t: Directory the input MIDI files to be converted\n-o --outputdir\t: Directory the csv files will be created\n-h --help\t: Prints this help message then exits\n-r --reverse\t: Converts csv to midi rather than midi to csv')




def csv2midi(inputdir, outputdir):
    for filename in os.listdir(inputdir):
        with open(inputdir +'/'+ filename, newline='') as csvfile:
            csvreader = csv.reader(csvfile, delimiter=',')
            mid = MidiFile()
            track = MidiTrack()
            mid.tracks.append(track)

            mkdir(outputdir)

            for row in csvreader:
                if (row[1] != "type"):
                    if(row[0] == "set_tempo"):
                        track.append(MetaMessage(row[0], tempo=int(row[1]), time=int(row[2])))
                    elif(row[0] == "time_signature"):
                        track.append(MetaMessage(row[0], numerator=int(row[1]), denominator=int(row[2]), clocks_per_click=int(row[3]), notated_32nd_notes_per_beat=int(row[4]), time=int(row[5])))
                    else:
                        track.append(Message(row[1], channel=int(row[0]), note=int(row[2]), velocity=int(row[3]), time=int(row[4])))               

            mid.save(outputdir + "/" + filename[:-4] + ".mid")

        print(filename + " Done")


            
def midi2csv(inputdir, outputdir):
    for filename in os.listdir(inputdir):

            mid = MidiFile(inputdir + "/" + filename)
            
            mkdir(outputdir)
                
            f = open(outputdir + "/" + filename[:-4] + ".csv", "w")

            f.write("channel,type,note,velocity,time\n")
            
            for i, track in enumerate(mid.tracks):
                for msg in track:
                    for x in range(0, 12):
                        if((hasattr(msg, 'channel')) and msg.channel == x):
                            if(hasattr(msg, 'note')):
                                f.write(str(msg.channel) + "," + msg.type + "," + str(msg.note) + "," + str(msg.velocity) + "," + str(msg.time) + "\n") 
                        if(not hasattr(msg, 'channel')):
                            if(msg.type == "set_tempo"):
                                f.write(str(msg.type) + "," + str(msg.tempo) + "," + str(msg.time) + "\n")
                            elif(msg.type == "time_signature"):
                                f.write(str(msg.type) + "," + str(msg.numerator) + "," + str(msg.denominator) + "," + str(msg.clocks_per_click) + "," + str(msg.notated_32nd_notes_per_beat) + "," + str(msg.time) + "\n")

            print(filename + " done")
            f.close()




def mkdir(newDir):
    try:
        os.mkdir(newDir)
    except OSError as error:  
        k = error


    

if __name__ == "__main__":
   main(sys.argv[1:])
