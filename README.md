# midi2csv
Converts midi files to csv using the mido library

usage: midi2csv.py [options] ... [-i --inputdir | -o --outputdir] [arg] ...

Options and arguments:

-i --inputdir   : Directory the input MIDI files to be converted

-o --outputdir  : Directory the csv files will be created

-h --help       : Prints this help message then exits
